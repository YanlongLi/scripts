#!/usr/bin/bash

if [ ! -z ${_LU_DEBUG_+x} ] || [ -z ${_LU_UTILS_+x} ]; then
########################################
__dir__="$(dirname "$0")"
source "$__dir__/LU_SCRIPTS/LU_ENV.sh"
export _LU_UTILS_="_LU_UTILS_"
########################################

LU_UnixPath2NetPath() {
  local path=$(realpath -m $1)
  echo "$path" | sed -e "s/$(echo $LU_MAPPEDBASE | sed -e 's_\/_\\\/_g')/$(echo $LU_BASE | sed -e 's/\\/\\\\/g')/I" -e 's/\//\\/g'
}

LU_NetPath2UnixPath(){
  local path=$1
  echo "$path" | sed -e "s/$(echo $LU_BASE | sed -e 's/\\/\\\\/g')/$(echo $LU_MAPPEDBASE | sed -e 's_\/_\\\/_g')/I" -e 's/\\/\//g'
}

############################################
# $1: InputClientId or "<locale> <version>"
# $2: Export Variable Name
############################################
TransformClientId() {
  if [ -z "$1" ]; then
    exit 1
  fi
  local ClientId="${1:-ClientID}"
  #function_body
  if [[ "$ClientId" =~ ' ' ]]; then
    local ClientId="Microsoft_Threshold_Shell_1_${ClientId%% *}_${ClientId##* }"
  fi
  if [ -z "$2" ]; then 
    echo $ClientId
  else
    local inputVar=$2
    eval $inputVar="'$ClientId'"
  fi
}

########################################
fi
