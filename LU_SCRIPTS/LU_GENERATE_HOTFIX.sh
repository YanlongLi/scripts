#!/usr/bin/bash

if [ ! -z ${_LU_DEBUG_+x} ] || [ -z ${_LU_GENERATE_HOTFIX_+x} ]; then
########################################
__dir__="$(dirname "$0")"
source "$__dir__/LU_SCRIPTS/LU_UTILS.sh"
export _LU_GENERATE_HOTFIX_="_LU_GENERATE_HOTFIX_"
########################################


GenerateHotfixXML() {
  local tofile=$1
  local outdir=$2
  local locale=$3
  local testfile=$4
  local modeldir=$5
  local clientid=$6
  #
cat > $tofile <<EOL
<?xml version="1.0" encoding="utf-8"?>
<ModelHotfixConfig>
  <Product>Threshold</Product>
  <Locale>${locale:0:2}-${locale:(-2)}</Locale>
  <Variant>Temp</Variant>
  <ClientId>$clientid</ClientId>
  <ModelDir>$modeldir</ModelDir>
  <AnalysisOutDir>\\\\YANLL-WORK\\Shared\\tmp\\hotfix\\</AnalysisOutDir>
  <OutputDir>$outdir\\out</OutputDir>
  <TestPath>$testfile</TestPath>
  <MLGToolDir>$LU_MLGTOOL_PATH</MLGToolDir>
</ModelHotfixConfig>
EOL
}

GenerateHotfixBat() {
  tofile=$1
  config=$2
  #
cat > $tofile <<EOL
$LU_MODEL_QUALIFIER_PATH\\bin\\LUModelHotfix.exe -c $config
PAUSE
EOL
  #
  unix2dos $tofile
}

########################################
fi
