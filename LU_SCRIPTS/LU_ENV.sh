#/usr/bin/bash

if [ ! -z ${_LU_DEBUG_+x} ] || [ -z ${_LU_ENV_+x} ]; then
########################################
export _LU_ENV_="_LU_ENV_"
__dir__="$(dirname "$0")"
########################################

# export LU_MODEL_QUALIFIER_PATH="\\\\suzhost-23\\users\\jiangazh\\LUTools\\CarinaModelQualifier"
export LU_MODEL_QUALIFIER_PATH='\\jianlij-work\Tools\ModelQualifier'
export LU_TOOL_PATH="\\\\suzhost-23\\users\\jiangazh\\LUTools\\CarinaModelQualifier"

export LU_MLGTOOL_PATH="$LU_TOOL_PATH\\Tools\\MlgTools"
export LU_QCSQUERYLABEL_PATH="$LU_TOOL_PATH\\Tools\\QCSQueryLabel"
export LU_SMEVAL_PATH="$LU_TOOL_PATH\\Tools\\smeval"
export LU_MODELEVALUATOR_PATH='\\YANLL-WORK\Threshold\tools\ModelEvaluator\ModelEvaluator.Console.exe'
export LU_MaxSessionBuffer='10000'

# export LU_BASE='\\SUZHOST-23\Threshold'
# export LU_MAPPEDBASE='/Threshold'
export LU_BASE='\\YANLL-WORK'
export LU_MAPPEDBASE='/d/Data'
########################################
fi
