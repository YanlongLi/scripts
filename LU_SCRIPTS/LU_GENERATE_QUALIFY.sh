#!/usr/bin/bash

if [ ! -z ${_LU_DEBUG_+x} ] || [ -z ${_LU_GENERATE_QUALIFY_+x} ]; then
########################################
__dir__="$(dirname "$0")"
source "$__dir__/LU_SCRIPTS/LU_UTILS.sh"
export _LU_GENERATE_QUALIFY_="_LU_GENERATE_QUALIFY_"
########################################

GenerateQualifyXML() {
  #function_body
  local tofile=$1
  local outdir=$2
  local locale=$3
  local testfile=$4
  local modeldir=$5
  local clientid=$6
  local variant=${7-"Temp"}
  #
cat > $tofile <<EOL
<?xml version="1.0" encoding="utf-8"?>
<QualifierConfig>
  <Product>Threshold</Product>
  <LocalDirectory>$outdir</LocalDirectory>
  <Locale>${locale:0:2}-${locale:(-2)}</Locale>
  <Data>
    <File path="$testfile" type="test" />
  </Data>
  <Model>
    <DecoderType>Cortana</DecoderType>
    <ModelDir>$modeldir</ModelDir>
    <ClientId>$clientid</ClientId>
    <Variant>$variant</Variant>
  </Model>
  <Tool>
    <ExperimentType>OfflineTool</ExperimentType>
    <FindFileAEther>ef313c2d-e448-4e16-b2eb-7700e3f53959</FindFileAEther>
    <QCSQueryLabelAEther>f298e0f8-d2d0-4d0c-88ec-69bf26ec792e</QCSQueryLabelAEther>
    <QCSEvaluatorAEther>7b2b651a-55ab-471a-89e2-57b77096f844</QCSEvaluatorAEther>
    <QCSQueryLabel>$LU_QCSQUERYLABEL_PATH</QCSQueryLabel>
    <QCSEvaluator>$LU_SMEVAL_PATH</QCSEvaluator>
  </Tool>
</QualifierConfig>
EOL
}

GenerateQualifyBat() {
  tofile=$1
  config=$2
  #
cat > $tofile <<EOL
$LU_MODEL_QUALIFIER_PATH\\LUModelQualifier.exe -c $config
PAUSE
EOL
  #
  unix2dos $tofile
}

########################################
fi
