#!/usr/bin/bash

if [ ! -z ${_LU_DEBUG_+x} ] || [ -z ${_LU_GENERATE_MODEL_EVALUATOR_+x} ]; then
########################################
__dir__="$(dirname "$0")"
source "$__dir__/LU_SCRIPTS/LU_UTILS.sh"
export _LU_GENERATE_MODEL_EVALUATOR_="_LU_GENERATE_MODEL_EVALUATOR_"
########################################

GenerateModelEvaluatorCompareBat() {
  local BaseLineModelPath=$1
  local BaselineVariant=$2
  local BaseLineClientId=$3
  local TestModelPath=$4
  local TestVariant=$5
  local TestClientId=$6
  local EvaluateName=$7
  local TestDataPath=$8
  local OutputPath=$9
  local Tofile=${10}
  #
cat > $Tofile <<EOL
$LU_MODELEVALUATOR_PATH --BaselineModel $BaseLineModelPath --BaselineVariant $BaselineVariant --BaselineClientId $BaseLineClientId --MaxSessionBuffer $LU_MaxSessionBuffer --TestModel $TestModelPath  --TestVariant $TestVariant --TestClientId $TestClientId --EvaluateName $EvaluateName --Output $OutputPath --TestDataPath $TestDataPath
@PAUSE
EOL
  #
  unix2dos $Tofile
}

GenerateModelEvaluatorBat() {
  local BaseLineModelPath=$1
  local BaselineVariant=$2
  local BaseLineClientId=$3
  local EvaluateName=$4
  local TestDataPath=$5
  local OutputPath=$6
  local Tofile=$7
  #
cat > $Tofile <<EOL
$LU_MODELEVALUATOR_PATH --BaselineModel $BaseLineModelPath --BaselineVariant $BaselineVariant --BaselineClientId $BaseLineClientId --MaxSessionBuffer $LU_MaxSessionBuffer --EvaluateName $EvaluateName --Output $OutputPath --TestDataPath $TestDataPath
@PAUSE
EOL
  #
  unix2dos $Tofile
}
########################################
fi
