#!/usr/bin/bash

__dir__="$(dirname "$0")"
source $__dir__/LU_SCRIPTS/LU_generate_config.sh

# LU_BASE='\\YANLL-WORK'
# LU_MAPPEDBASE='/d/Data'

read -e -p "Input BaseLineModelPath: " BaseLineModelPath
read -e -p "Input BaselineVariant (Temp): " BaselineVariant
read -e -p "Input BaseLineClientId (ClientID): " BaseLineClientId
read -e -p "Input TestModelPath (): " TestModelPath
if [ ! -z $TestModelPath ]; then
  read -e -p "Input TestVariant (Temp): " TestVariant
  read -e -p "Input TestClientId (ClientID): " TestClientId
fi
read -e -p "Input EvaluateName (DoFullEvaluation,DoDomainEvaluation,DoIntentEvaluation,DoSlotEvaluation): " EvaluateName
read -e -p "Input OutputPath (output): " OutputPath
read -e -p "Input TestDataPath: " TestDataPath
read -e -p "Input tofile (ModelEvaluator.bat): " tofile

BaseLineModelPath=${BaseLineModelPath:-Temp}
BaselineVariant=${BaselineVariant:-Temp}
BaseLineClientId=${BaseLineClientId:-ClientID}
BaselineClientId="`TransformClientId "$BaseLineClientId"`"
# TestModelPath=${# TestModelPath:-}
TestVariant=${TestVariant:-Temp}
TestClientId=${TestClientId:-ClientID}
TestClientId="`TransformClientId "$TestClientId"`"
EvaluateName=${EvaluateName:-DoFullEvaluation}
OutputPath=${OutputPath:-output}
# TestDataPath=${# TestDataPath:-}
tofile=${tofile:-ModelEvaluator.bat}

if [ -z $TestModelPath ]; then
  echo $tofile
  GenerateModelEvaluatorBat \
    "\"`LU_UnixPath2NetPath $(realpath $BaseLineModelPath)`\"" \
    "$BaselineVariant" \
    "$BaseLineClientId" \
    "$EvaluateName" \
    "\"`LU_UnixPath2NetPath $(realpath $TestDataPath)`\"" \
    "\"`LU_UnixPath2NetPath $(realpath $OutputPath)`\"" \
    "$tofile"
else
  GenerateModelEvaluatorCompareBat \
   "\"`LU_UnixPath2NetPath $(realpath $BaseLineModelPath)`\"" \
    "$BaselineVariant" \
    "$BaseLineClientId" \
    "\"`LU_UnixPath2NetPath $(realpath $TestModelPath)`\"" \
    "$TestVariant" \
    "$TestClientId" \
    "$EvaluateName" \
    "\"`LU_UnixPath2NetPath $(realpath $TestDataPath)`\"" \
    "\"`LU_UnixPath2NetPath $(realpath $OutputPath)`\"" \
    "$tofile"
fi

exit 0
