#!/usr/bin/bash

####################
domainDir=`realpath domain`
intentDir=`realpath intent`
slotDir=`realpath slot`
outDir=`realpath model`

if [ ! -d "$domainDir" ]; then
  echo "no domain model, $domainDir, exit"
  exit 1
fi

if [ -d "$outDir" ]; then
  rm -rf "$outDir"
fi
mkdir -p "$outDir"

tmp_pre="$outDir/_tmp_pre.ini"
cat >> "$tmp_pre" <<EOL
[global]
GlobalTopoSort=true
ReadApplicationHostRequest=true
ApplicationHostRequestScenarioName=QAS5
ApplicationHostRequestServiceName=XAPQuServiceAnswer

[scenario:QAS]
schema=QASv5

[clients]
ClientID=ClientID

[query_domains:ClientID]
EOL

if [ -d "$domainDir" ]; then
  for domain in `ls -d $domainDir/*/`; do
    d=`basename $domain`
    domainIni="$outDir/_$d.ini"
    preIni="$outDir/_${d}_preprocess.ini"
    ini="$domainDir/$d/$d.QueryProcessingConfiguration.ini"
    if [ "$d" == "model" ]; then
      continue
    fi
    for file in `ls $domainDir/$d/ipe_lu_Microsoft_Threshold_Shell_1_*` ; do
      cp $file $outDir/
    done
    for file in `ls $domainDir/$d/generic_wordbreaker*`; do
      cp -f $file $outDir/
    done
    grep -E "^${d}" "$domainDir/$d/$d.QueryProcessingConfiguration.ini" >> "$tmp_pre"
    sed -n -e "/\[qd_ipe_lu_Microsoft_Threshold_Shell_1_${d}_.*_query_analysis\]/,/domainclassifier=qd_ipe_lu_Microsoft_Threshold_Shell_1_${d}_.*_domain_domainclassifier/ p" $ini >> "$preIni"
    echo ";; $d domain" >> "$domainIni"
    sed -n -e "/\[qd_ipe_lu_Microsoft_Threshold_Shell_1_${d}_.*_preprocess_featurizer\]/,$ p" $ini >> "$domainIni"
    echo "$d domain done"
  done
fi

if [ -d "$intentDir" ]; then
  for domain in `ls -d $intentDir/*/`; do
    d=`basename $domain`
    domainIni="$outDir/_$d.ini"
    preIni="$outDir/_${d}_preprocess.ini"
    ini="$intentDir/$d/$d.QueryProcessingConfiguration.ini"
    for file in `ls $intentDir/$d/ipe_lu_Microsoft_Threshold_Shell_1_*` ; do
      cp -f $file $outDir/
    done
    sed -n -e "/\[qd_ipe_lu_Microsoft_Threshold_Shell_1_${d}_.*_query_analysis\]/,/\[qd_ipe_lu_Microsoft_Threshold_Shell_1_${d}_.*_preprocess_featurizer\]/ p" $ini \
      | sed -n -e '3,$ p' | head --lines=-1 >> "$preIni"
    echo ";; $d intent" >> "$domainIni"
    sed -n -e "/\[qd_ipe_lu_Microsoft_Threshold_Shell_1_${d}_.*_intents_.*_featurizer\]/,$ p" $ini >> "$domainIni"
  done
fi

if [ -d "$slotDir" ]; then
  for domain in `ls -d $slotDir/*/`; do
    d=`basename $domain`
    domainIni="$outDir/_$d.ini"
    preIni="$outDir/_${d}_preprocess.ini"
    ini="$slotDir/$d/$d.QueryProcessingConfiguration.ini"
    for file in `ls $slotDir/$d/ipe_lu_Microsoft_Threshold_Shell_1_*` ; do
      cp -f $file $outDir/
    done
    sed -n -e "/\[qd_ipe_lu_Microsoft_Threshold_Shell_1_${d}_.*_query_analysis\]/,/\[qd_ipe_lu_Microsoft_Threshold_Shell_1_${d}_.*_preprocess_featurizer\]/ p" $ini \
      | sed -n -e '3,$ p' | head --lines=-1 >> "$preIni"
    echo ";; $d slot" >> "$domainIni"
    sed -n -e "/\[qd_ipe_lu_Microsoft_Threshold_Shell_1_${d}_.*_slots_.*_featurizer\]/,$ p" $ini >> "$domainIni"
  done
fi

outIni="${outDir}/Temp.QueryProcessingConfiguration.ini"

cat "$tmp_pre" > "$outIni"
rm -f "$tmp_pre"
echo "" >> "$outIni"
echo "" >> "$outIni"
for domain in `ls -d $domainDir/*/`; do
  d=`basename $domain`
  domainIni="$outDir/_$d.ini"
  preIni="$outDir/_${d}_preprocess.ini"
  echo ";; $d begin" >> ${outIni}
  cat "$preIni" >> ${outIni}
  cat "$domainIni" >> ${outIni}
  echo ";; $d end" >> ${outIni}
  rm -f "$domainIni"
  rm -f "$preIni"
done

dos2unix "$outIni"
